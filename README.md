# **[EN] Installer for french pokerstars players on Linux with Lutris**

Install lutris and wine on Arch Linux
``` 
sudo pacman -S lutris wine
```
Install lutris and wine on Ubuntu/Mint
```
sudo apt install lutris wine
```
Or download at [https://lutris.net/downloads](https://lutris.net/downloads)

Download json file
``` 
git clone https://gitlab.com/slowsaz/pokerstarsLinuxFr
cd pokerstarsLinuxFr
```
Install pokerstars
``` 
lutris -i pokerstars-eu_fr.json
```
Ignore the different alerts and click to install.

The installation can be exit with error but don't pay attention.

# **[FR] Installeur pour les utilisateurs français de pokerstars sur Linux avec Lutris**

Installez lutris et wine sur Arch Linux
``` 
sudo pacman -S lutris wine
```
Installez lutris et wine sur Ubuntu/Mint
```
sudo apt install lutris wine
```
Ou téléchargez le à [https://lutris.net/downloads](https://lutris.net/downloads)

Téléchargez le fichier json
``` 
git clone https://gitlab.com/slowsaz/pokerstarsLinuxFr
cd pokerstarsLinuxFr
```
Installez pokerstars
``` 
lutris -i pokerstars-eu_fr.json
```
Ignorez les différentes alertes et cliquez sur install.

L'installation peut se terminer avec une erreur mais n'y prêtez pas attention.